** This is a simple login and registration script based on PHP & MySQL. This script uses PHP session to store user details across multiple PHP pages to verify if the user is logged in or not. This script has minimal HTML/PHP/MySQL code which is easy to understand. **

* Download this project into a separate folder
* Import database.sql file in a database 'register_login_db'
* Now run this folder's index.php file. You will find 'login' & 'registration' link in this page.
* Register some users and try login with them.
* You will be able to access page-1.php and page-2.php content only after login.
* You can logout/login many times to test it.

This is an easy login script prepared by tutorialsclass.com. You can find step by step description about the login script in following tutorial.

Website & Tutorial Link: https://tutorialsclass.com/code/simple-php-login-registration-script